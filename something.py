#!/usr/bin/env python3

from tkinter import *
import os

OPTIONS = ['Rock', 'Rap', 'Country', 'Pop']

master = Tk()

def callback():
	os.system('./main < input')

entry = Text(master, height=2, width=25)
entry.grid(row=1, column=0) 

t = Label(master, text = 'Genre', font=('Times', 14, 'bold'))
t.grid(row=0, column=3)

variable = StringVar(master)
variable.set(OPTIONS[0]) # default value

w = OptionMenu(master, variable, *OPTIONS)
w.grid(row=1, column=3)

next = Button(master, text="Next", command=callback)
next.grid(row=3, column=0)
like = Button(master, text="Like")
like.grid(row=3, column=1)
skip = Button(master, text="Skip")
skip.grid(row=3, column=2)
quit = Button(master, text="Quit", command=master.destroy)
quit.grid(row=3, column=3)

mainloop()
