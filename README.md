Final Project
=============
# Smart Shuffle

## Instructions
	- Enter the number of songs you would like to have in your playlist
	- Enter the song title, artist, album, and genre in that order each 
	on their own line
	- The playlist will shuffle and the user will have several options:
		stop: entering "stop" will stop the playlist and quit
		next: entering "next" will go to the next song
		skip: entering "skip" will go to the next song and lower 
			  the priorities of the songs from the same artist and 
			  genre of the song being skipped, and then re-shuffle the 
			  playlist accordingly.
		like: entering "like" will raise the priorities of the songs from 
			  the same artist and genre of the song being liked, and 
			  re-shuffle the playlist accordingly.
		request: entering "request" will allow the user to enter the name of 
				 the song they would like to hear next. Once it is 
				 entered, the requested song will play.



## Contributions:
	Broke up into pairs to work towards a solution on each 
	function/program. Split the work equally between each other 
	and switched up pairs we worked together with.
