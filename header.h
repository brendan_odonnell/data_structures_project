#include <iostream>
#include <string>
#include <climits>
#include <cstdlib>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <iomanip>
#include <time.h>
using namespace std;

/* Song Struct */

struct Song {
	string 	name;
	string 	album;
	string 	artist;
	string 	genre;
};

/* Node Struct */

struct Node {
	Song 	s;
	Node 	*next;
	double 	priority;
};

/* Linked List Header */

class Linked_List 
{
	public:
		Linked_List();
		~Linked_List();
		void push_front(Song);
		void remove(string);	
		void bubsort(Node *);
		Node *msort(Node *);
		void split(Node *, Node *&, Node *&);
		Node *merge(Node *, Node *);
		Node *qsort(Node *head);
		void partition(Node *head, Node *pivot, Node *&left, Node *&right);
		Node *concatenate(Node *left, Node *right);
		void print(bool);
		bool update(Song, string);
		Song pop();
		bool next(Song);
		bool skip(Song);
		bool like(Song);
		bool request(Song);
		bool streq(string, string);
		Node *head;
};

