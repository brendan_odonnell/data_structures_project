CXX=		g++
CXXFLAGS=	-g -gdwarf-2 -std=gnu++11 -Wall
SHELL=		bash
TARGETS=	main
LD=			g++
LDFLAGS=	-L.
AR=			ar
ARFLAGS=	rcs

all:	$(TARGETS)

%.o:	%.cpp header.h
	$(CXX) $(CXXFLAGS) -o $@ -c $<

#listlib.a:	linkedlist.o
#	$(AR) $(ARFLAGS) $@ $^

main:	main.o linkedlist.o
	$(LD) $(LDFLAGS) -o $@ $^


clean:
	rm -f *.o *.a $(TARGETS)
