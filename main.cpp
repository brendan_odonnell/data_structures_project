#include "header.h"

int main(int argc, char *argv[]){
	char method = 'M'; // We have three sorting methods: merge sort, quick sort, and bubble sort
	bool verbose = 0; // and a verbose option, defaults are set here
	if (argc > 1){ // If flags are selected, the commmand line is parsed
		for (int i = 1; i < argc; i++){
			if (!strcmp(argv[i], "BUBBLE")){
				method = 'B';
			} else if (!strcmp(argv[i], "QUICK")){
				method = 'Q';
			} else if (!strcmp(argv[i], "MERGE")){
				method = 'M';
			} else if (!strcmp(argv[i], "V")){
				verbose = true;
			} else {
				cout << "Incorrect sorting method suggested. Exitting program."<<endl;
				return EXIT_FAILURE;
			}
		}
	}
	// Initialize all variables to be used
	Song s;
	string name, artist, album, genre;
	Linked_List playlist;
	int num;
	// Asks user for input
	cout << "Input number of songs to be added to your playlist: ";
	cin >> num;
	// Clears cin
	cin.ignore();
	//Takes in all data for all songs to be added to playlist and pushes them to the front of the linked list
	for (int i = 0; i < num; i++){
		getline(cin, name);
		getline(cin, artist);
		getline(cin, album);
		getline(cin, genre);
		s.name = name;
		s.artist = artist;
		s.album = album;
		s.genre = genre;
		playlist.push_front(s);
	}
	bool play = true;
	string choice;
	// Determines which method to sort with
	switch (method){
		case 'M':
			playlist.head = playlist.msort(playlist.head);
			break;
		case 'B':
			playlist.bubsort(playlist.head);
			break;
		case 'Q':
			playlist.head = playlist.qsort(playlist.head);
			break;
	}
	cout << endl;
	//Prints list
	playlist.print(verbose);
	while(play) {
		Song result = playlist.pop();
		cout << "Playing now: "<<result.name << " by " << result.artist << endl;
		cout << "Waiting for user action"<<endl;
		cout << "Options: stop, next, skip, like, or request"<<endl;
		cout << endl;
		getline(cin, choice);
		cout << "Choice is: "<< choice <<endl;
		play = playlist.update(result, choice);
		switch (method){
			case 'M':
				playlist.head = playlist.msort(playlist.head);
				break;
			case 'B':
				playlist.bubsort(playlist.head);
				break;
			case 'Q':
				playlist.head = playlist.qsort(playlist.head);
				break;
		}
		playlist.print(verbose);
	}
	return EXIT_SUCCESS;
}
