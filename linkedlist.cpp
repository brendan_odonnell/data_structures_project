#include "header.h"
using namespace std;

Linked_List::Linked_List() {
	head = nullptr;
	srand(time(NULL));
}

Linked_List::~Linked_List() {
	Node *next = nullptr;
	for (Node *curr = head; curr != nullptr ; curr = next){
		next = curr->next;
		delete curr;
	}
}

void Linked_List::push_front(Song s) {
	if (head == nullptr){
		head = new Node{s, nullptr, INT_MAX};
	} else {
		Node *curr = head;
		head = new Node{s, curr, double(rand() % 50 + 1)};
	}
}
void Linked_List::remove(string s) {

	if (s.compare(head->s.name) == 0){
		Node *temp = head;
		head = head->next;
		delete temp;
		
	} else {
		Node *curr = head;
		bool found = false;
		while (!found) {
			if (s.compare(curr->s.name) == 0){
				found = true;
				Node *temp = curr;
				curr = curr->next;
				delete temp;
			} else {
				curr = curr->next;
			}
		}
	}
}

Node *Linked_List::msort(Node *head) {
	if (head == nullptr || head->next == nullptr){
		return nullptr;
	} else {
		Node *left = nullptr;
		Node *right = nullptr;
		split(head, left, right);
		if (left->next != nullptr){
			left = msort(left);
		}
		if (right->next != nullptr){
			right = msort(right);
		}
		head = merge(left, right);
		return head;	
	}
}

void Linked_List::split(Node *head, Node *&left, Node *&right) {
	Node *fast = head;
	Node *slow = head;
	Node *tail = nullptr;
	Node *temp = nullptr;
	while (fast->next != nullptr) {
		tail = slow;
		temp = fast->next;
		if (temp->next != nullptr){
			fast = temp->next;
			slow = slow->next;
		} else {
			fast = temp;
			slow = slow->next;
		}
	}
	tail->next = nullptr;
	left = head;
	right = slow;
}

Node *Linked_List::merge(Node *left, Node *right) {
	Node *head;
	bool match = (left->priority > right->priority);
	if (left && right){
		if (match) {
			head = left;
			left = left->next;
		} else if (!match) {
			head = right;
			right = right->next;
		}
	}
	Node *curr = head;
	while (left && right) {
		match = (left->priority > right->priority);
		if (match) {
			curr->next = left;
			left = left->next;
		} else if (!match) {
			curr->next = right;
			right = right->next;
		}
		curr = curr->next;
	}
	while (left && !right) {
		curr->next = left;
		left = left->next;
		curr = curr->next;
	}
	while (!left && right) {
		curr->next = right;
		right = right->next;
		curr = curr->next;
	}
	return head;
}

void Linked_List::bubsort(Node *head){
    
    bool swapped  = true;
    while (swapped) {
        /* Initialization */
        swapped = false;
        Node *curr = head->next;
        Node *prev = head;
    
        /* Bubble Sort */
        while (curr != nullptr) {
            if (curr->priority > prev->priority) {
    
               /* Swap Data */
               int temp_priority    = curr->priority;
               Song temp_song       = curr->s;
               
	       curr->priority       = prev->priority;
               curr->s		        = prev->s;    
               
	       prev->priority       = temp_priority;
               prev->s           = temp_song;    
               swapped = true;
          	}   

            /* Move Pointers */
            prev = curr;    
            curr = curr->next;
        }   
    }    
}
Node *Linked_List::qsort(Node *head){
	if (head == nullptr){
		return nullptr;
	} else {
		Node *left = nullptr;
		Node *right = nullptr;
		partition(head->next, head, left, right);
		left = qsort(left);
		right = qsort(right);
		head->next = right;
		return concatenate(left, head);
	}
	return nullptr;
}

void Linked_List::partition(Node *head, Node *pivot, Node *&left, Node *&right){
	left = nullptr;
	right = nullptr;
	Node *curr = head;
	Node *next;
	bool match;
	while (curr != nullptr){
		next = curr->next;
		match = (pivot->priority < curr->priority);
		if (match){
			curr->next = right;
			right = curr;
		} else {
			curr->next = left;
			left = curr;
		}
		curr = next;
	}
}

Node *Linked_List::concatenate(Node *left, Node *right) {
	Node *curr = left;
	if (left == nullptr){
		return right;
	}
	while (curr->next != nullptr){
		curr = curr->next;
	}
	curr->next = right;
	return left;
}


void Linked_List::print(bool verbose){
	Node *curr = head;
	cout << setw(25) << left << "Song Name" << setw(25) << left << "Artist" << setw(25) << "Album" << setw(25) << "Genre";
	if (verbose){
		cout << setw(25) << "Priority" <<endl;
	} else {
		cout << endl;
	}

	while (curr != nullptr){
		cout << setw(25) << curr->s.name << setw(25) << left << curr->s.artist << setw(25)  << curr->s.album << setw(25) << curr->s.genre;
		if (verbose){
			cout << setw(25) << curr->priority << endl;
		} else {
			cout << endl;
		}
		curr = curr->next;
	}
	cout << endl;
}
bool Linked_List::update(Song s, string action) {
	if (!action.compare("stop")){
		return false;
	} else if (!action.compare("next")){
		return next(s);
	} else if (!action.compare("skip")){
		return skip(s);
	} else if (!action.compare("like")){
		return like(s);
	} else if (!action.compare("request")){
		return request(s);
	} else {
		return true;
	}
}

Song Linked_List::pop() {
	return head->s;
}
bool Linked_List::streq(string s, string p){
	return !s.compare(p);
}

bool Linked_List::next(Song s){
	Node *curr = head;
	head->priority = 1;
	curr = curr->next;
	while (curr != nullptr){
		if (s.artist == curr->s.artist){
			curr->priority *= 1.25;
		}
		if (s.genre == curr->s.genre){
			curr->priority *= 1.25;
		}
		curr = curr->next;
	}
	return true;
}

bool Linked_List::skip(Song s){
        Node *curr = head;
	head->priority = 1;
	curr = curr->next;
	while (curr != nullptr){
		if (s.artist == curr->s.artist){
			curr->priority *= .75;
			if (curr->priority < 1) {
				curr->priority = double(rand() % 50 + 1);
			}
		}
		if (s.genre == curr->s.genre){
			curr->priority *= .75;
			if (curr->priority < 1) {
				curr->priority = double(rand() % 50 + 1);
			}
		}
		curr = curr->next;
	}
	return true;
}

bool Linked_List::like(Song s){
	Node *curr = head;
	head->priority = head->priority * .5;
	curr = curr->next;
	while (curr != nullptr){
		if (s.artist == curr->s.artist){
			curr->priority *= 1.75;
		}
		if (s.genre == curr->s.genre){
			curr->priority *= 1.75;
		}
		curr = curr->next;
	}
	return true;
}

bool Linked_List::request(Song s){
	Node *curr = head;
	string str;
	cout << "What song would you like to play next? ";
	getline(cin, str);
	cin.ignore();
	while (curr != nullptr){
		if (curr->s.name == str){
			curr->priority = INT_MAX;
			return true;
		}
		curr = curr->next;
	}
	return true;
}
